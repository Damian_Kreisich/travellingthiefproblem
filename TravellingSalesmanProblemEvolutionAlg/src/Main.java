import java.io.File;


public class Main {
    public static void main(String[] args) {
        Loader l = new Loader(
                    new File("C:\\Users\\Master\\IdeaProjects\\travellingthiefproblem\\TravellingSalesmanProblemEvolutionAlg\\src\\Datasets\\hard_0.ttp"));
        l.load();

        TravellingThief.setConstraints(l.getConstraints());
        Area area = new Area(l.getCities(), l.getItems());

        l.close();
        l = null;
        // Loader is unusable now
        Generations generations = new Generations(area);
        generations.startEvolution();
    }
}

