public class Item {
    private int index;
    private int profit;
    private int weight;
    private int node_number;

    public Item(int index, int profit, int weight, int node_number) {
        this.index = index;
        this.profit = profit;
        this.weight = weight;
        this.node_number = node_number;
    }
    public double countRatio(){
        if (weight != 0) {
            return profit / (weight * 1.0);
        }
        else {
            return 0;
        }
    }
    public String toString() {
        return "" + index;
    }

    public int getIndex() {
        return index;
    }

    public int getNode_number() {
        return node_number;
    }

    public int getWeight() {
        return weight;
    }

    public int getProfit() {
        return profit;
    }
}
