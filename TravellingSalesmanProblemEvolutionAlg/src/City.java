public class City implements Comparable{
    private int index;
    private int xCoordinate;
    private int yCoordinate;

    public City(int index, int xCoordinate, int yCoordinate) {
        this.index = index;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }
    public String toString(){
        return index +"";
    }
    public boolean equals(Object o){
        return index == ((City)o).index;
    }

    public int getIndex() {
        return index;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    @Override
    public int compareTo(Object o) {
        if (equals(o))
            return 0;
        else
            if (index > ((City)o).index)
                return 1;
            else
                return -1;
    }

    @Override
    public int hashCode() {
        return new Integer(index).hashCode();
    }
}
