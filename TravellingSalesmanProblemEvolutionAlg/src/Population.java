import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Population implements Cloneable{
    private List<TravellingThief> thieves;
    private Area areaOfStealing;
    private int ammountOfIndividuals;


    private double bestResult;
    private TravellingThief bestThief;
    private double averageResult;
    private double worstResult;

    public Population(int ammountOfIndividuals, Area area){
        this.ammountOfIndividuals = ammountOfIndividuals;
        areaOfStealing = area;
        thieves = new ArrayList<>(ammountOfIndividuals);
    }


    public void initialize(){
        for(int i = 0; i < ammountOfIndividuals; i++){
            thieves.add(i, new TravellingThief());
            thieves.get(i).initializeRandomThief(areaOfStealing);
        }
    }

    public void evolve(){
        thieves = tournamentSelection();
        //thieves = rouletteSelection();
        Randomizer rand = new Randomizer();
        int randomForCouple;
        int splitPosition;
        for(int i = 0; i < ammountOfIndividuals; i++){
            if (Constants.CROSSING_CHANCE > rand.doubleInRange(0, 1)){
                randomForCouple = rand.intInRange(0, thieves.size());
                splitPosition = rand.intInRange(0,thieves.get(i).getVisitedCities().size());
                thieves.get(i).cross(thieves.get(randomForCouple), splitPosition);
            }
            if (Constants.MUTATION_CHANCE > rand.doubleInRange(0, 1)){
                 thieves.get(i).mutate();
            }
        }
        stealLikeThieves();
    }

    private void stealLikeThieves(){
        for (TravellingThief thief : thieves){
            thief.growUpAsAThief(areaOfStealing);
        }
    }

    public void countFitness(){
        Double singleFitness;
        TravellingThief actualThief;

        bestResult = Double.NEGATIVE_INFINITY;
        double sumForAverageResult = 0;
        averageResult = 0;
        worstResult = Double.POSITIVE_INFINITY;

        for(int i = 0; i < ammountOfIndividuals; i++){
            actualThief = thieves.get(i);
            if (!actualThief.isCounted()) {
                singleFitness = actualThief.countStolenGoodsValue() - actualThief.countRoadTime(areaOfStealing);
                actualThief.setFitness(singleFitness);
                actualThief.setCounted(true);
            }
            else {
                singleFitness = actualThief.getFitness();
            }
            sumForAverageResult += singleFitness;
            if (bestResult <= singleFitness){
                bestResult = singleFitness;
                bestThief = thieves.get(i);
            }
            if (worstResult >= singleFitness){
                worstResult = singleFitness;
            }
        }
        averageResult = sumForAverageResult/ammountOfIndividuals;
    }
    public List<TravellingThief> rouletteSelection(){
        List<TravellingThief> result = new ArrayList<>();
        Randomizer rand = new Randomizer();
        List<Double> fitnessesWithOffset = new ArrayList<>();

        double offset = 0;

        if (worstResult < 0){
            offset = -worstResult;
        }

        double sumFitness = 0;

        for (TravellingThief thief : thieves){
            sumFitness += thief.getFitness() + offset;
        }

        int i = 0;
        while (result.size() < ammountOfIndividuals){
            i = i%thieves.size();
            if(rand.intInRange(0, 1) < (thieves.get(i).getFitness() + offset)/sumFitness){
                try {
                    result.add((TravellingThief)thieves.get(i).clone());
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
            i++;
        }

        return result;
    }

    public List<TravellingThief> tournamentSelection(){
        List<TravellingThief> participants;
        List<TravellingThief> result = new ArrayList<>();
        while (result.size() < ammountOfIndividuals){
            participants = selectForTournament();
            try {
                result.add((TravellingThief)resolveTournament(participants).clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    private List<TravellingThief> selectForTournament(){
        List<TravellingThief> result = new ArrayList<>();
        Randomizer rand = new Randomizer();
        int i = 0;
        while (result.size() != Constants.TOURNAMENT_SIZE){
            result.add(thieves.get(rand.intInRange(0, thieves.size())));
        }
        return result;
    }
    private TravellingThief resolveTournament(List<TravellingThief> participants){
        double actual;
        double max = Double.NEGATIVE_INFINITY;
        TravellingThief winner = null;
        for(TravellingThief thief : participants){
            actual = thief.getFitness();
            if (actual > max){
                max = actual;
                winner = thief;
            }
        }
        return winner;
    }
    public String toString(){
        return areaOfStealing.toString() + "\n" + thieves.toString();
    }

    public double getBestResult() {
        return bestResult;
    }

    public TravellingThief getBestThief() {
        return bestThief;
    }

    public double getAverageResult() {
        return averageResult;
    }

    public double getWorstResult() {
        return worstResult;
    }
    public Object clone() throws CloneNotSupportedException{
        Population p = (Population)super.clone();
        return p;
    }
}
