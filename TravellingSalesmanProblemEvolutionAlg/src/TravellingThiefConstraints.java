public class TravellingThiefConstraints {
    private double minSpeed;
    private double maxSpeed;
    private int knapsackCapacity;

    public TravellingThiefConstraints(double minSpeed, double maxSpeed, int knapsackCapacity) {
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
        this.knapsackCapacity = knapsackCapacity;
    }

    public TravellingThiefConstraints() {
    }

    public double getMinSpeed() {
        return minSpeed;
    }

    public void setMinSpeed(double minSpeed) {
        this.minSpeed = minSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getKnapsackCapacity() {
        return knapsackCapacity;
    }

    public void setKnapsackCapacity(int knapsackCapacity) {
        this.knapsackCapacity = knapsackCapacity;
    }
}
