import java.util.Random;

public class Randomizer {
    private Random rand;
    public Randomizer(){
        rand = new Random();
    }
    public int intInRange(int start, int end){
        return rand.ints(start,end).findAny().getAsInt();
    }
    public double doubleInRange(double start, double end){
        return rand.doubles(start, end).findAny().getAsDouble();
    }
}
