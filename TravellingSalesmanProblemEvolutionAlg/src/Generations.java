public class Generations {
    public static Logger logger;

    private Population [] populations;

    private double definiteEvolutionResult;
    private TravellingThief definiteAlphaIndividual;

    public Generations(Area area){
        populations = new Population[Constants.AMMOUNT_OF_POPULATIONS];
        populations[0] = new Population(Constants.AMMOUNT_OF_INDIVIDUALS, area);
        definiteEvolutionResult = Double.NEGATIVE_INFINITY;
        if (logger == null){
            logger = new Logger("log.csv");
        }
    }

    public void startEvolution(){
        //start first population
        //operations on first population
        System.out.println("initialize");
        populations[0].initialize();
        populations[0].countFitness();
        definiteEvolutionResult = populations[0].getBestResult();
        definiteAlphaIndividual = populations[0].getBestThief();
        String line;
        //log initial data
        logger.logLine("Nr. Populacji|najlepszy|sredni|najgorszy \n");
        logger.logLine(logger.prepareForSinglePopulationLog(0, populations[0].getBestResult(),
                populations[0].getAverageResult(), populations[0].getWorstResult()));

        for(int i = 1; i < Constants.AMMOUNT_OF_POPULATIONS; i++){
            try {
                populations[i] = (Population)populations[i-1].clone();
                populations[i].evolve();
                populations[i-1]=null;
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }

            populations[i].countFitness();
            //log actual data
            line = logger.prepareForSinglePopulationLog(i, populations[i].getBestResult(),
                    populations[i].getAverageResult(), populations[i].getWorstResult());
            System.out.println(line);
            logger.logLine(line);
            if (populations[i].getBestResult() > definiteEvolutionResult){
                definiteEvolutionResult = populations[i].getBestResult();
                definiteAlphaIndividual = populations[i].getBestThief();
            }

        }
        logger.logLine(definiteAlphaIndividual.toString());
        System.out.println(definiteAlphaIndividual.toString());
        logger.logLine(definiteEvolutionResult + "");
        logger.close();
    }
}
