import java.util.*;
import java.util.stream.Collectors;

public class TravellingThief implements Cloneable{

    private static TravellingThiefConstraints constraints;

    private List<City> visitedCities;
    private List<Item> knapsackedItems;
    private int actualCapacity;
    private double fitness;

    private boolean counted = false;

    public boolean isCounted() {
        return counted;
    }

    public void setCounted(boolean counted) {
        this.counted = counted;
    }

    public static TravellingThiefConstraints getConstraints() {
        return constraints;
    }

    public List<Item> getKnapsackedItems() {
        return knapsackedItems;
    }

    public void setKnapsackedItems(List<Item> knapsackedItems) {
        this.knapsackedItems = knapsackedItems;
    }

    public int getActualCapacity() {
        return actualCapacity;
    }

    public void setActualCapacity(int actualCapacity) {
        this.actualCapacity = actualCapacity;
    }

    public static void setConstraints(TravellingThiefConstraints constraints) {
        TravellingThief.constraints = constraints;
    }

    public TravellingThief(){
        knapsackedItems = new ArrayList<>();
    }

    public void initializeRandomThief(Area area){
        visitedCities = new ArrayList<>(area.getCities());
        Collections.shuffle(visitedCities);

        growUpAsAThief(area);

    }

    public void growUpAsAThief(Area area){
        Item item;
        for (City city : visitedCities) {
            item = stealWithBestRatio(area, city);
            knapsackedItems.add(item);
            if (item != null)
                actualCapacity += item.getWeight();
        }
    }

    private Item stealWithBestRatio(Area area, City city){
        List<Item> items = area.getItems(city);
        Item resultItem = null;

        double bestRatio = 0;
        double ratio;
        for(Item item : items){
            ratio = item.countRatio();
            if (ratio > bestRatio && actualCapacity+item.getWeight() < constraints.getKnapsackCapacity()) {
                bestRatio = ratio;
                resultItem = item;
            }
        }
        return resultItem;
    }
    public double countStolenGoodsValue(){ //g(x)
        double result = 0;
        for(Item item: knapsackedItems){
            if (item != null)
                result = result + item.getProfit();
        }
        return result;
    }

    public double countRoadTime(Area area){
        double result = 0;
        actualCapacity = 0;
        double actualVelocity = 0;
        Item pickedItem;
        int ammountOfCities = visitedCities.size();
        City cityB;
        City cityA;

        for(int i = 1; i <= ammountOfCities; i++) {
            pickedItem = knapsackedItems.get(i - 1);

            if (pickedItem != null) {
                actualCapacity += pickedItem.getWeight();
            }
            actualVelocity = countActualVelocity(actualCapacity);
            if (i != ammountOfCities) {
                cityB = visitedCities.get(i);
                cityA = visitedCities.get(i - 1);
            }
            else { //way back
                cityB = visitedCities.get(0);
                cityA = visitedCities.get(i - 1);
            }
            result = result + area.getDistance(cityA, cityB) / actualVelocity;
        }
        return result;

    }

    private double countActualVelocity(double actualKnapsackWeight){
        double maxSpeed = constraints.getMaxSpeed();
        double velocity = maxSpeed - actualKnapsackWeight*
                (maxSpeed-constraints.getMinSpeed())/constraints.getKnapsackCapacity();
        return velocity;
    }

    @Override
    public String toString() {
        return "Fitness: " + fitness + "\nVisited:" + visitedCities.toString() + "\n" + "Stolen:" + knapsackedItems.toString() + "\n";
    }

    public void mutate(){
        Randomizer rand = new Randomizer();
        int position1 = rand.intInRange(0, visitedCities.size());
        int position2 = rand.intInRange(0, visitedCities.size());

        Collections.swap(visitedCities, position1, position2);
        counted = false;

    }

    public void cross(TravellingThief thief, int splitPoint){
        int finalCity = visitedCities.size();
        List<City> resultFirstList = new ArrayList<>(visitedCities.subList(0, splitPoint));
        List<City> resultSecondList = new ArrayList<>(thief.visitedCities.subList(splitPoint, finalCity));
        resultFirstList.addAll(thief.visitedCities.subList(splitPoint, finalCity));
        resultSecondList.addAll(visitedCities.subList(0, splitPoint));

        resolveCityConflict(this, resultFirstList);
        resolveCityConflict(thief, resultSecondList);
        counted = false;
        thief.counted = false;
    }

    private static void resolveCityConflict(TravellingThief thief, List <City> cities){
        List<City> temporalList = cities.stream().distinct().collect(Collectors.toList());
        for(City city : thief.visitedCities){ //use old visited cities to have all the objects that are needed
            if (!temporalList.contains(city)){
                temporalList.add(city);
            }
        }
        thief.visitedCities = temporalList;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        TravellingThief thief = (TravellingThief)super.clone();
        thief.visitedCities = new ArrayList<>(visitedCities);
        thief.knapsackedItems = new ArrayList<>();
        thief.actualCapacity = 0;

        return thief;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public List<City> getVisitedCities() {
        return visitedCities;
    }

    public void setVisitedCities(List<City> visitedCities) {
        this.visitedCities = visitedCities;
    }
}
