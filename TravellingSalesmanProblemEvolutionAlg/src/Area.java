import java.util.*;
import java.util.stream.Collectors;

public class Area {
    private Map<City, List<Item>> areaMap;
    private double [][] distances;

    public Area(List<City> cities, List<Item> items) {
        areaMap = new HashMap<>();
        distances = new double[cities.size()+1][cities.size()+1];
        List<Item> itemsInCity;
        for (City cityA : cities) {
            itemsInCity = items.stream().filter(item -> item.getNode_number() == cityA.getIndex()).collect(Collectors.toList());
            areaMap.put(cityA, itemsInCity);
            for (City cityB : cities) {
                if (!cityA.equals(cityB)) {
                    distances[cityA.getIndex()][cityB.getIndex()] = countDistance(cityA, cityB);
                }
            }
        }
    }

    private double countDistance(City cityA, City cityB){
        double xDistance = cityA.getxCoordinate() - cityB.getxCoordinate();
        double yDistance = cityA.getyCoordinate() - cityB.getyCoordinate();

        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    public List<City> getCities() {
        return new ArrayList<>(areaMap.keySet());
    }

    public List<Item> getItems(City fromCity) {
        return areaMap.get(fromCity);
    }

    public String toString(){
        return "Area: \n" + areaMap.toString();
    }

    public double getDistance(City cityA, City cityB){
        return distances[cityA.getIndex()][cityB.getIndex()];
    }
}
