public class Constants {
    public static final int AMMOUNT_OF_INDIVIDUALS = 1000;
    public static final int AMMOUNT_OF_POPULATIONS = 1000;

    public static final double CROSSING_CHANCE = 0.7;
    public static final double MUTATION_CHANCE = 0.15;

    public static final int TOURNAMENT_SIZE = 100;
}
