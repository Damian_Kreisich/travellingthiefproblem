import java.io.*;

public class Logger {
    private BufferedWriter bw;
    public Logger(String fileName){ //BAD PRACTICE
        try {
            bw = new BufferedWriter(new FileWriter(new File(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String prepareForSinglePopulationLog(int nrPop, double bestResult, double averageResult, double worstResult){
        return nrPop + "|" + String.format("%.2f", bestResult) + "|" + String.format("%.2f",averageResult) + "|" +
                String.format("%.2f",worstResult) + "\n";
    }
    public void logLine(String line){
        try {
            bw.write(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void close(){
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
