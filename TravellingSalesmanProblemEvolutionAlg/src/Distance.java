public class Distance {
    private City cityA;
    private City cityB;
    private double distance;

    public Distance(City cityA, City cityB){
        this.cityA = cityA;
        this.cityB = cityB;
        distance = countDistance();
    }
    private double countDistance(){
        double xDistance = cityA.getxCoordinate() - cityB.getxCoordinate();
        double yDistance = cityA.getyCoordinate() - cityB.getyCoordinate();

        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }
    //possible to improve, cut half of Distance set
    public boolean equals(Object o){
        return cityA.equals(((Distance)o).cityA) && cityB.equals(((Distance)o).cityB);
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public int hashCode() {
        return cityA.hashCode() * cityB.hashCode();
    }
}
