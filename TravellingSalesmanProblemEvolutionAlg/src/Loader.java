import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Loader {
    private static final int LINES_TO_SKIP_AT_START = 2;
    private static final int LINES_TO_SKIP_UNNEEDED_VALUES = 3;
    private static final int LINES_TO_SKIP_HEADER = 1;
    private static final int META_VALUE_POSITION = 1;
    private static final int X_COORDINATE_POSITION = 1;
    private static final int Y_COORDINATE_POSITION = 2;
    private static final int INDEX_POSITION = 0;
    private static final int PROFIT_POSITION = 1;
    private static final int WEIGHT_POSITION = 2;
    private static final int NODE_NUMBER_POSITION = 3;

    private File fileInput;
    private BufferedReader bfReader;

    private TravellingThiefConstraints constraints;
    private List<City> cities;
    private List<Item> items;


    public Loader(File fileInput) {
        this.fileInput = fileInput;
        cities = new ArrayList<>();
        items = new ArrayList<>();
        constraints = new TravellingThiefConstraints();
    }

    public void load(){
        String line;
        String[] splitTable;
        int dimensions;
        int numberOfItems;
        int knapsackCapacity;
        double minSpeed;
        double maxSpeed;
        try {
            bfReader = new BufferedReader(new FileReader(fileInput));
            if (bfReader.ready()){
                skipLines(LINES_TO_SKIP_AT_START);

                line = bfReader.readLine();
                splitTable = line.split(":\\s+");
                dimensions = Integer.parseInt(splitTable[META_VALUE_POSITION]);

                line = bfReader.readLine();
                splitTable = line.split(":\\s+");
                numberOfItems = Integer.parseInt(splitTable[META_VALUE_POSITION]);

                line = bfReader.readLine();
                splitTable = line.split(":\\s+");
                knapsackCapacity = Integer.parseInt(splitTable[META_VALUE_POSITION]);
                constraints.setKnapsackCapacity(knapsackCapacity);

                line = bfReader.readLine();
                splitTable = line.split(":\\s+");
                minSpeed = Double.parseDouble(splitTable[META_VALUE_POSITION]);
                constraints.setMinSpeed(minSpeed);

                line = bfReader.readLine();
                splitTable = line.split(":\\s+");
                maxSpeed = Double.parseDouble(splitTable[META_VALUE_POSITION]);
                constraints.setMaxSpeed(maxSpeed);

                skipLines(LINES_TO_SKIP_UNNEEDED_VALUES);

                getCitiesFromFile(dimensions);

                skipLines(LINES_TO_SKIP_HEADER);

                getItemsFromFile(numberOfItems);


            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void skipLines(int numberOfLines) throws IOException{
        for (int i = 0; i < numberOfLines; i++){
            bfReader.readLine();
        }
    }

    private void getCitiesFromFile(int dimensions) throws IOException{
        String line;
        String[] splitTable;

        City city;
        int index;
        int xCoord;
        int yCoord;

        for(int i = 0; i < dimensions; i++){
            line = bfReader.readLine();
            splitTable = line.split("\\s+");

            index = Integer.parseInt(splitTable[INDEX_POSITION]);
            if(splitTable[X_COORDINATE_POSITION].indexOf(".") != -1)
                xCoord = Integer.parseInt(splitTable[X_COORDINATE_POSITION].
                    substring(0, splitTable[X_COORDINATE_POSITION].indexOf(".")));
            else
                xCoord = Integer.parseInt(splitTable[X_COORDINATE_POSITION]);
            if(splitTable[Y_COORDINATE_POSITION].indexOf(".") != -1)
                yCoord = Integer.parseInt(splitTable[Y_COORDINATE_POSITION].
                    substring(0, splitTable[Y_COORDINATE_POSITION].indexOf(".")));
            else
                yCoord = Integer.parseInt(splitTable[Y_COORDINATE_POSITION]);

            city = new City(index, xCoord, yCoord);
            cities.add(city);
        }
    }
    private void getItemsFromFile(int numberOfItems) throws IOException{
        String line;
        String[] splitTable;

        Item item;
        int index;
        int profit;
        int weight;
        int nodeNumber;

        for(int i = 0; i < numberOfItems; i++){
            line = bfReader.readLine();
            splitTable = line.split("\\s+");

            index = Integer.parseInt(splitTable[INDEX_POSITION]);
            profit = Integer.parseInt(splitTable[PROFIT_POSITION]);
            weight = Integer.parseInt(splitTable[WEIGHT_POSITION]);
            nodeNumber = Integer.parseInt(splitTable[NODE_NUMBER_POSITION]);

            item = new Item(index, profit, weight, nodeNumber);
            items.add(item);

        }
    }
    public TravellingThiefConstraints getConstraints() {
        return constraints;
    }

    public List<City> getCities() {
        return cities;
    }

    public List<Item> getItems() {
        return items;
    }

    public void close(){
        try {
            bfReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
